using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


public class LevelManager : MonoBehaviour
{
	public MindItem[] markedItems = new MindItem[2];

	/// <summary>
	/// Singleton pattern instance.
	/// </summary>
	static public LevelManager instance = null;
	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Debug.Log("There is already an instance of LevelManager registered!");
		}
	}

	private void Update()
	{
		if(Input.GetKeyDown(KeyCode.F5))
		{
			LoadNewScene("MainScene");
		}

		if(markedItems[0] != null && markedItems[1] != null)
		{
			CombineItems();
		}
	}

	public bool IsCurrentCombinationValid(out MindItem[] combinesTo, out string combinationText)
	{
		for(int i = 0; i < markedItems[0].combinesWith.Count; ++i)
		{
			if(markedItems[0].combinesWith[i].itemID == markedItems[1].itemID)
			{
				Debug.Log("Testing if recognizes: Works");
				//SpawnMindItem(markedItems[0].combinesTo[i].gameObject);
				combinesTo = markedItems[0].combinesTo[i].items;
				//DefaultOnScreenPrint.instance.ChangeText(markedItems[0].combinationText[i]);
				combinationText = markedItems[0].combinationText[i];
				return true;
			}
		}
		for(int i = 0; i < markedItems[1].combinesWith.Count; ++i)
		{
			if(markedItems[1].combinesWith[i].itemID == markedItems[0].itemID)
			{
				Debug.Log("Testing if recognizes: Works");
				//SpawnMindItem(markedItems[1].combinesTo[i].gameObject);
				combinesTo = markedItems[1].combinesTo[i].items;
				//DefaultOnScreenPrint.instance.ChangeText(markedItems[1].combinationText[i]);
				combinationText = markedItems[1].combinationText[i];
				return true;
			}
		}
		combinesTo = null;
		combinationText = null;
		return false;
	}

	public void CombineItems()
	{
		MindItem[] combinesTo;
		string combinationText;

		
		if(IsCurrentCombinationValid(out combinesTo, out combinationText))
		{
			foreach(MindItem combinationResult in combinesTo) { SpawnMindItem(combinationResult.gameObject); }
			DefaultOnScreenPrint.instance.ChangeText(combinationText);

			Destroy(markedItems[0].gameObject);
			Destroy(markedItems[1].gameObject);
		}
		else
		{
			DefaultOnScreenPrint.instance.ChangeText("It seems your subconsciousness recejects your conscious decision that these two things combine into something relevant.");
		}
		markedItems[0].UnmarkThis();
		markedItems[1].UnmarkThis();
	}

	public void SpawnMindItem(GameObject prefabToSpawn)
	{
		GameObject tmp = Instantiate(prefabToSpawn);
	}

	public void GameOver()
	{
		EndScreen.instance.Show();
	}

	public void ExitGame()
	{
		Application.Quit();
	}
	public void LoadNewScene(string sceneName)
	{
		SceneManager.LoadScene(sceneName);
		IngameMenu.instance.Hide();
	}
}