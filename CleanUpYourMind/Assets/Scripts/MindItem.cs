﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct MindItems
{
	public MindItem[] items;
}

public class MindItem : Interactable2D
{
	public int itemID;
	
	public List<MindItem> combinesWith = new List<MindItem>();
	public List<MindItems> combinesTo;
	public string[] combinationText;

	public override void MarkThis()
	{
		base.MarkThis();

		if(LevelManager.instance.markedItems[0] == null)
		{
			LevelManager.instance.markedItems[0] = this;
		}
		else if(LevelManager.instance.markedItems[1] == null)
		{
			LevelManager.instance.markedItems[1] = this;
		}
		else
		{
			Debug.LogWarning("Marked items has both on non NULL but an additional MindItem is trying to get marked!");
		}
	}
	public override void UnmarkThis()
	{
		base.UnmarkThis();

		if(LevelManager.instance.markedItems[0] == this)
		{
			LevelManager.instance.markedItems[0] = null;
		}
		else if(LevelManager.instance.markedItems[1] == this)
		{
			LevelManager.instance.markedItems[1] = null;
		}
	}
}