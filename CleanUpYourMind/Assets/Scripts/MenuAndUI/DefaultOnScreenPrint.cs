﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefaultOnScreenPrint : UICanvasManager
{
	[SerializeField] public Text textDisplay;

	/// <summary>
	/// Singleton pattern instance.
	/// </summary>
	static public DefaultOnScreenPrint instance = null;
	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Debug.Log("There is already an instance of DefaultOnScreenPrint registered!");
		}
	}

	public void ChangeText(string newText)
	{
		textDisplay.text = newText;
	}

	public override void Show()
	{
		base.Show();
	}
	public override void Hide()
	{
		base.Hide();
	}
}
