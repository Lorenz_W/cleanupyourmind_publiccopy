using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UICanvasManager : MonoBehaviour
{
	[SerializeField] protected Canvas UICanvas;
	protected bool isVisible = false;


	virtual protected void Start()
	{
		Hide();
	}

	virtual protected void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(this.isVisible)
			{
				Hide();
			}
		}
	}

	virtual public void Show()
	{
		this.isVisible = true;
		this.UICanvas.gameObject.SetActive(true);
	}
	virtual public void Hide()
	{
		this.isVisible = false;
		this.UICanvas.gameObject.SetActive(false);
	}
}