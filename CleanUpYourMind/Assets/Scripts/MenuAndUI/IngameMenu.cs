﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameMenu : UICanvasManager
{
	/// <summary>
	/// Singleton pattern instance.
	/// </summary>
	static public IngameMenu instance = null;
	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Debug.Log("There is already an instance of IngameMenu registered!");
		}
	}

	override protected void Start()
	{
		Show();
	}
	protected override void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(!this.isVisible)
			{
				Show();
			}
			else
			{
				Hide();
			}
		}
	}

	public override void Show()
	{
		DefaultOnScreenPrint.instance.Hide();
		Time.timeScale = 0;
		base.Show();
	}
	public override void Hide()
	{
		DefaultOnScreenPrint.instance.Show();
		Time.timeScale = 1;
		base.Hide();
	}
}