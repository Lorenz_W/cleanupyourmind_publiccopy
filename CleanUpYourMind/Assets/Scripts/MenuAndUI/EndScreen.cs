﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreen : UICanvasManager
{
	[SerializeField] Image iconPrefab;
	/// <summary>
	/// Singleton pattern instance.
	/// </summary>
	static public EndScreen instance = null;
	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Debug.Log("There is already an instance of EndScreen registered!");
		}
	}

	protected override void Update()
	{
		if(isVisible && Input.GetKeyDown(KeyCode.Escape))
		{
			LevelManager.instance.LoadNewScene("MainScene");
		}
	}

	public override void Show()
	{
		base.Show();
		MindItem[] mindItemsLeft = FindObjectsOfType<MindItem>();
		foreach(MindItem mindItemLeft in mindItemsLeft)
		{
			Image tmp = Instantiate<Image>(iconPrefab, UICanvas.transform);
			tmp.sprite = mindItemLeft.GetComponent<SpriteRenderer>().sprite;
			tmp.color = mindItemLeft.GetComponent<SpriteRenderer>().color;
		}
	}
}	
