﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D), typeof(SpriteRenderer))]
public class Interactable2D : MonoBehaviour
{
	[SerializeField] protected string interactionText;
	[SerializeField] protected SpriteRenderer highlight;
	bool isMarked = false;
    public float timer = 0f;

	private void OnMouseOver()
	{
        
        if (Input.GetMouseButtonDown(1) && Time.timeScale != 0f)
		{
			DefaultOnScreenPrint.instance.ChangeText(interactionText);
		}
		else if(Input.GetMouseButtonDown(0) && Time.timeScale != 0f)
		{
            timer = Time.time;

			if(isMarked)
			{
				UnmarkThis();
			}
			else
			{
				MarkThis();
			}
		}
        if (Input.GetMouseButtonDown(0) && Time.timeScale != 0f) {
			DragNDrop2D.instance.TryDragObject(this.gameObject);
        }
    }
	

	virtual public void MarkThis()
	{
		isMarked = true;
		highlight.enabled = true;
	}
	virtual public void UnmarkThis()
	{
		isMarked = false;
		highlight.enabled = false;
	}
}
