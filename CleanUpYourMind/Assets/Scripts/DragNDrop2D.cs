﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragNDrop2D : MonoBehaviour
{
	[SerializeField] List<string> dragableObjectTags;
	bool isDragging = false;
	GameObject toBeDragged;
	/// <summary>
	/// Vector between camera and toBeDragged on click.
	/// </summary>
	Vector3 offsetValue;

	/// <summary>
	/// Singleton pattern instance.
	/// </summary>
	static public DragNDrop2D instance = null;
	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Debug.Log("There is already an instance of DragNDrop2D registered!");
		}
	}

    void Update()
    {
		if(isDragging && Input.GetMouseButtonUp(0))
		{
			isDragging = false;
			if (toBeDragged.GetComponent<MindItem>().timer <= Time.time - 0.2f) {
				toBeDragged.GetComponent<MindItem>().UnmarkThis();
			}
		}

		if(isDragging)
		{
			if(toBeDragged == null)
			{
				isDragging = false;
				return;
			}
			toBeDragged.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) - new Vector3(0.0f,0.0f, Camera.main.gameObject.transform.position.z);
		}
	}

	/// <summary>
	/// Replacement until I get raycast from mouse working for 2D colliders at some point in time.
	/// </summary>
	/// <param name="toDrag"></param>
	public void TryDragObject(GameObject toDrag)
	{
		Debug.Log("Drag event actually gets fired");

		toBeDragged = toDrag;
		if(toBeDragged != null && dragableObjectTags.Contains(toBeDragged.tag))
		{
			isDragging = true;
		}
	}
}
